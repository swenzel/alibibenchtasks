#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

NEVENTS=200 SYSTEM=PbPb ENGINE=TGeant3 . ${DIR}/simrecoworkflow_template.sh
