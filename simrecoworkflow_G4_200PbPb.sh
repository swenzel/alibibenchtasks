#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "${DIR}"

NEVENTS=200 SYSTEM=PbPb ENGINE=TGeant4 . ${DIR}/simrecoworkflow_template.sh
