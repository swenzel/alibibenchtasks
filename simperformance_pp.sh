#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "${DIR}"

NEVENTS=10 SYSTEM=pp GEN=pythia8 . ${DIR}/simperformance_template.sh
