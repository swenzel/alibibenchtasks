#!/usr/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cp "${SCRIPTDIR}"/foo.ipynb .
jupyter nbconvert foo.ipynb --execute --to notebook
