#!/bin/bash

#################################################
# Task performing an analysis of simulation steps
#################################################

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# This needs MCSTEPLOGGER; so we probe for it first
[[ ! -d "${MCSTEPLOGGER_ROOT}" ]] && echo "MCSTEPLOGGER_ROOT not set" && (return 1 2>/dev/null || exit 1)

today=`date +%d-%m-%Y-%H:%M`
STEPLOGCOMMON="MCSTEPLOG_TTREE=ON LD_PRELOAD=${MCSTEPLOGGER_ROOT}/lib/libMCStepLoggerIntercept.so"

generators="pythia8"
engines="TGeant3 TGeant4"
modulelist="CAVE PIPE MAG ABSO SHIL DIPO COMP FRAME HALL EMC TPC TRD ITS TOF PHS HMP MFT MCH SHIL CPV FV0 FT0 FDD MID ZDC"

for ZDCCONF in "wZDC" "noZDC"; do
  SKIPMODULES="" && [[ "${ZDCCONF}" == "noZDC" ]] && SKIPMODULES="--skipModules ZDC"
  for GEN in ${generators}; do
    NEVENTS="1" && [[ "${GEN}" == "pythia8hi" ]] && NEVENTS="1"

    for ENG in ${engines}; do
      CONF="${ENG}_N${NEVENTS}_G${GEN}_${ZDCCONF}"
      STEPFILE="steps_${CONF}.root"
      OUTPREFIX="o2sim_${CONF}"

      # launch simulation job using the step logger
      # the idea is to do all conf in parallel
      COMMAND="MCSTEPLOG_OUTFILE=${STEPFILE} ${STEPLOGCOMMON} o2-sim-serial -n ${NEVENTS} -e ${ENG} -g ${GEN} ${SKIPMODULES} -o ${OUTPREFIX} --seed 1 &> log_${CONF}"
      eval "${COMMAND}" &
    done
  done
done
# wait for all sim jobs
wait

analysistask() {
  CUT=$1
  STEPFILE=$2
  OUTDIR=$3
  LOGFILE=$4

  COMMAND="" && [[ ! "${CUT}" == "" ]] && COMMAND="MCSTEPCUT=${CUT} "
  COMMAND="${COMMAND} mcStepAnalysis analyze -f ${STEPFILE} -o ${OUTDIR} -l stepana &> ${LOGFILE}"
  eval ${COMMAND}

  # do some conversion to text formats
  root -q -b -l ${SCRIPTDIR}/helpers/TH1ToCSV.C\(\"${OUTDIR}/SimpleStepAnalysis/Analysis.root\",\"nStepsPerMod\"\) | grep -v "Processing" | grep -v ",0,0" > ${OUTDIR}_stepspermod.csv

  root -q -b -l ${SCRIPTDIR}/helpers/TH1ToCSV.C\(\"${OUTDIR}/SimpleStepAnalysis/Analysis.root\",\"nStepsPerVol\"\) | grep -v "Processing" | grep -v ",0,0" > ${OUTDIR}_stepspervol.csv

  root -q -b -l ${SCRIPTDIR}/helpers/TH1ToCSV.C\(\"${OUTDIR}/SimpleStepAnalysis/Analysis.root\",\"nStepsPerPDG\"\) | grep -v "Processing" | grep -v ",0,0" > ${OUTDIR}_stepsperpdg.csv

  root -q -b -l ${SCRIPTDIR}/helpers/TH1ToCSV.C\(\"${OUTDIR}/SimpleStepAnalysis/Analysis.root\",\"nSecondariesPerMod\"\) | grep -v "Processing" | grep -v ",0,0" > ${OUTDIR}_secondariespermod.csv
}

open_sem(){
    mkfifo pipe-$$
    exec 3<>pipe-$$
    rm pipe-$$
    local i=$1
    for((;i>0;i--)); do
        printf %s 000 >&3
    done
}

# run the given command asynchronously and pop/push tokens
run_with_lock(){
    local x
    # this read waits until there is something to read
    read -u 3 -n 3 x && ((0==x)) || exit $x
    (
     ( "$@"; )
    # push the return code of the command to the semaphore
    printf '%.3d' $? >&3
    )&
}

# allow 8 parallel analysis jobs (due to memory)
# this is a reasonable number for alibi
open_sem 8

# build up task list
for ZDCCONF in "wZDC" "noZDC"; do
  for GEN in ${generators}; do
    for ENG in ${engines}; do
      NEVENTS="1" && [[ "${GEN}" == "pythia8hi" ]] && NEVENTS="1"
      CONF="${ENG}_N${NEVENTS}_G${GEN}_${ZDCCONF}"
      STEPFILE="steps_${CONF}.root"
      OUTDIR="GlobalStepAnalysis_${CONF}"

      # analyse global data
      run_with_lock analysistask "" ${STEPFILE} ${OUTDIR} "log_anastep_${CONF}" 

      # analyse again all data on a per module level
      # std::string expr = "user_cut(const o2::StepInfo &step, 
      #                    const std::string &volname,                                       
      #                    const std::string &modname,                                       
      #                    int pdg, o2::StepLookups* lookup)"
      for det in ${modulelist}; do
        CUT="'return modname.compare(\""
        CUT="${CUT}${det}\") == 0;'"
        run_with_lock analysistask "${CUT}" "${STEPFILE}" "${OUTDIR}_only_${det}" "log_anastep_${CONF}_only_${det}" 
      done
      # wait for parallel analysis jobs spawned within the inner body 
    done
  done
done



# make metrics for InfluxDB -- just global level
HOST=`hostname`
for ZDCCONF in "wZDC" "noZDC"; do
  for GEN in ${generators}; do
    for ENG in ${engines}; do
      NEVENTS="20" && [[ "${GEN}" == "pythia8hi" ]] && NEVENTS="1"
      CONF="${ENG}_N${NEVENTS}_G${GEN}_${ZDCCONF}"
      OUTDIR="GlobalStepAnalysis_${CONF}"

      TAG="conf=${CONF},host=${HOST}${ALIPERF_ALIDISTCOMMIT:+,alidist=$ALIPERF_ALIDISTCOMMIT}${ALIPERF_O2COMMIT:+,o2=$ALIPERF_O2COMMIT}"

      steps_metrics="mcsteps,${TAG} ";
      STEPCSVFILE=${OUTDIR}_stepspermod.csv
      steps_total=0.

      second_metrics="mcsecondtracks,${TAG} ";
      SECCSVFILE=${OUTDIR}_secondariespermod.csv
      second_total=0.

      for det in ${modulelist}; do
        STEPNUMBER=`grep "${det}" $STEPCSVFILE | sed 's/,/ /g' | awk '//{print $2}'`
        steps_metrics="${steps_metrics}${det}=${STEPNUMBER:-0},"
        steps_total=`echo "${steps_total} ${STEPNUMBER:-0}" | awk '//{print $1+$2}'`

        SECNUMBER=`grep "${det}" $SECCSVFILE | sed 's/,/ /g' | awk '//{print $2}'`
        second_metrics="${second_metrics}${det}=${SECNUMBER:-0},"
        second_total=`echo "${second_total} ${SECNUMBER:-0}" | awk '//{print $1+$2}'`
      done
      steps_metrics="${steps_metrics}total=${steps_total} "
      second_metrics="${second_metrics}total=${second_total} "
      echo "${steps_metrics}" >> metrics.dat
      echo "${second_metrics}" >> metrics.dat
    done
  done
done

# submit stuff to InfluxDB
submit_to_AliPerf_InfluxDB metrics.dat


# make jupyter notebook reports
for ZDCCONF in "wZDC" "noZDC"; do
  for GEN in ${generators}; do
      NEVENTS="1" && [[ "${GEN}" == "pythia8hi" ]] && NEVENTS="1"

      for det in "NONE" ${modulelist}; do
        echo "DET=${det}"
        CUTSTRING="" && [[ ! "${det}" == "NONE" ]] && CUTSTRING="only_${det}"
        G4BASE="GlobalStepAnalysis_TGeant4_N${NEVENTS}_G${GEN}_${ZDCCONF}${CUTSTRING:+_$CUTSTRING}"
        G3BASE="GlobalStepAnalysis_TGeant3_N${NEVENTS}_G${GEN}_${ZDCCONF}${CUTSTRING:+_$CUTSTRING}"
        CONF="${GEN}_${ZDCCONF}_N${NEVENTS}${CUTSTRING:+_$CUTSTRING}"
        TITLE="MC Step report - CONF ${CONF} - O2Tag ${ALIPERF_O2COMMIT:-nan} AlidistTag ${ALIPERF_ALIDISTCOMMIT:-nan}"
        (
          export G3STEPMODFILE=${G3BASE}_stepspermod.csv
          export G4STEPMODFILE=${G4BASE}_stepspermod.csv
          export G3STEPVOLFILE=${G3BASE}_stepspervol.csv
          export G4STEPVOLFILE=${G4BASE}_stepspervol.csv
          export G3STEPPDGFILE=${G3BASE}_stepsperpdg.csv
          export G4STEPPDGFILE=${G4BASE}_stepsperpdg.csv

          # setup the title of the notebook
          sed s/TITLEPLACEHOLDER/"${TITLE}"/ ${SCRIPTDIR}/helpers/StepReport.ipynb > StepReport_${CONF}.ipynb
          jupyter nbconvert StepReport_${CONF}.ipynb --ExecutePreprocessor.timeout=-1 --execute --to notebook
        )

      done
  done
done
wait

# copy step data to EOS
TARGETDIR=${today}_ALIDIST:${ALIPERF_ALIDISTCOMMIT}_O2:${ALIPERF_O2COMMIT}
STEPDIR=/eos/user/a/aliperf/simulation/stepanalysis

#eos mkdir ${STEPDIR}/${TARGETDIR}
#eos mkdir ${STEPDIR}/${TARGETDIR}/data
#eos mkdir ${STEPDIR}/${TARGETDIR}/reports

#eos cp *.nbconvert.ipynb ${STEPDIR}/${TARGETDIR}/reports
#eos cp steps*.root ${STEPDIR}/${TARGETDIR}/data
#eos cp *.csv ${STEPDIR}/${TARGETDIR}/data
